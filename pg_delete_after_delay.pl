#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;

use Getopt::Long;
use File::Spec;
use File::Path qw(make_path);
use Config::YAML;
use Mojo::Collection 'c';
use Mojo::File;
use Mojo::Util qw(encode);
use Mojo::URL;
use Mojo::Pg;
use Etherpad;
use Data::Dumper;

my $c = Config::YAML->new(config => '/etc/pg_delete_after_delay.yml');

my $instance;
my ($help, $verbose, $daemon, $dry_run, $sleep, $expired) = (0, 0, 0, 0, 600, 0);
shift @ARGV if ($ARGV[0] eq '--');
GetOptions(
    'instance|i=s' => \$instance,
    'verbose|v'    => \$verbose,
    'daemon|d'     => \$daemon,
    'dry-run|n'    => \$dry_run,
    'sleep|s=i'    => \$sleep,
    'help|h'       => \$help,
);

print_usage(0) if $help;
print_usage(1) unless ($instance && $c->{$instance});
print_usage(5) unless ($c->{$instance} && $c->{$instance}->{pg});

my $pg = Mojo::Pg->new(pg_url($c->{$instance}->{pg}));

my $ec = Etherpad->new(
    url    => $c->{$instance}->{url},
    apikey => $c->{$instance}->{key},
);
print_usage(2) unless $ec->check_token();

my $delay  = $c->{$instance}->{del};
print_usage(3) unless ($delay && $delay > 0);

my $text   = $c->{$instance}->{text} || 'The content of this pad has been deleted since it was older than the configured delay.';

my $dir = File::Spec->catdir($c->{$instance}->{dir}, 'deleted_pads');
make_path $dir unless (-e $dir);
print_usage(4) unless (-d $dir && -w $dir);

if ($daemon) {
    while (1) {
        say 'Launching deletion loop' if $verbose;
        delete_after_delay();
        sleep $sleep;
    }
} else {
    say 'Launching deletion loop' if $verbose;
    delete_after_delay();
}

sub delete_after_delay {
    say 'Getting pads list' if $verbose;
    my $pads   = c($ec->list_all_pads());
    $pads->each(
        sub {
            my ($e, $num) = @_;

            if (defined($e) && $e ne '') {
                say 'Checking pad '.$e if $verbose;

                my $last_edit = $ec->get_last_edited($e);
                my $revs      = $ec->get_revisions_count($e);

                if ($last_edit && $revs) {
                    my $time  = time * 1000;
                    if (($time - $last_edit) > ($delay * 1000)) {
                        say 'Will delete '.$e.' (time diff: '.($time - $last_edit).')' if $verbose;
                        say sprintf 'Expired pad: %s', ($e) if (!$verbose && $dry_run);
                        $expired++;

                        unless ($dry_run) {
                            say 'Getting HTML version of '.$e if $verbose;

                            my $html = $ec->get_html($e);
                            if ($html) {
                                my $length = length($e);
                                my @dirs = ($c->{$instance}->{dir}, 'deleted_pads');
                                if ($length == 1) {
                                    my $a = $e;
                                    $a = '_' if $a eq ' ';
                                    push @dirs, $a;
                                } elsif ($length == 2) {
                                    my $a = substr($e, 0, 1);
                                    $a = '_' if $a eq ' ';
                                    my $b = substr($e, 1, 1);
                                    $b = '_' if $b eq ' ';
                                    push @dirs, $a, $b;
                                } else {
                                    my $a = substr($e, 0, 1);
                                    $a = '_' if $a eq ' ';
                                    my $b = substr($e, 1, 1);
                                    $b = '_' if $b eq ' ';
                                    my $c = substr($e, 2, 1);
                                    $c = '_' if $c eq ' ';
                                    push @dirs, $a, $b, $c;
                                }
                                my $dir = File::Spec->catdir(@dirs);
                                make_path $dir unless (-d $dir);
                                my $file = File::Spec->catfile($dir, $e.'-'.$time.'.html');

                                say 'Putting HTML version of '.$e.' in '.$file if $verbose;

                                Mojo::File->new($file)->spurt(encode('UTF-8', $html));

                                say 'Deleting '.$e if $verbose;

                                delete_pad($e);
                            }
                        } else {
                            say 'Not deleting '.$e.' since dry-running' if $verbose;
                        }
                    }
                }
                sleep 2;
            }
        }
    );
    say sprintf 'Number of expired pads: %d', $expired if (!$verbose && $dry_run);
    $expired = 0;
}

sub pg_url {
    my $conf = shift;
    ## Let's go
    my $addr  = Mojo::URL->new
                    ->scheme('postgresql')
                    ->host($conf->{host})
                    ->path('/'.$conf->{database});
    $addr->port($conf->{port}) if defined $conf->{port};
    my $user = (defined $conf->{user}) ? $conf->{user} : '';
    my $pwd  = (defined $conf->{pwd}) ? $conf->{pwd} : '';
    $addr->userinfo($user.':'.$pwd) if ($user && $pwd);
    return $addr->to_unsafe_string;
}

sub delete_pad {
    my $pad = shift;
    my $tx = $pg->db->begin;
    my $readonly = $pg->db->query('SELECT value FROM store WHERE key = ?', 'pad2readonly:'.$pad)->hashes->first->{value};
    $pg->db->query('DELETE FROM store WHERE key = ? OR key = ? OR key = ? OR key LIKE ? OR key LIKE ?',
        'pad:'.$pad,
        'pad2readonly:'.$pad,
        'readonly2pad:'.$readonly,
        'pad:'.$pad.':revs:%',
        'pad:'.$pad.':chat:%');
    $tx->commit;
}

sub print_usage {
    my $reason = shift;
    if ($reason == 1) {
        print <<EOF;
You don't have specified any instance or the specified instance is not in the configuration file.

EOF
    } elsif ($reason == 2) {
        print <<EOF;
Unable to contact the instance API. Check your API key in the configuration file and your Etherpad instance.

EOF
    } elsif ($reason == 3) {
        print <<EOF;
The delay is not configured for the specified instance or is less than zero.

EOF
    } elsif ($reason == 4) {
        print <<EOF;
Aborting since $dir is not writable.

EOF
    } elsif ($reason == 5) {
        print <<EOF;
Aborting since missing postgresql informations in the configuration file.

EOF
    }
    print <<EOF;
delete_after_delay (c) Framasoft 2016 GPLv3

Delete pads fram an Etherpad instance if the last edition is older than the configured delay.

Usage: pg_delete_after_delay [--instance|-i <instance>] [--verbose|-v] [--dry-run|-n] [--daemon|-d] [--sleep|-s <delay in seconds>] [--help|-h]

Options:
    --instance|-i <instance>    name of the instance to check
    --verbose|-v                verbose output (by default, pg_delete_after_delay only prints errors)
    --dry-run|-n                check pads, but do nothing
    --daemon|-d                 run in loop indefinitely (by default, auto_pgpadrepair does only one check on all pads and exits)
    --sleep|-s <delay>          delay, in seconds, between loops (works only with --daemon|-d)
    --help|h                    prints this help and exits

Available Etherpad instances:
EOF
    for my $i (keys %{$c}) {
        say sprintf '    - %s', $i unless ($i =~ m#^(_infile|_outfile|_strict)$#);
    }
    exit 1;
}
